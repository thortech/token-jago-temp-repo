var JAGOTOKENV2 = artifacts.require("./JAGOTOKENV2");

module.exports = function(deployer) {
  const name_ = "JAGOTOKEN";
  const symbol_ = "JAGO";
  const marketing_ = '0x22F14E5333Ceb95644Af4717837B238235d30122';
  deployer.deploy(JAGOTOKENV2, name_, symbol_, marketing_);
}
