const JAGOTOKENV2 = artifacts.require("JAGOTOKENV2");
const marketing_ = '0x476d4BcF6D445B6b2d46e1ED791f54FA681CE5F8';
/*const deploy = async function() {
  const name_ = "JAGOTOKEN";
  const symbol_ = "JAGOT";
  const marketing_ = '0x476d4BcF6D445B6b2d46e1ED791f54FA681CE5F8';
  JAGO = await JAGOTOKENV2.new(name_, symbol_, marketing_);
};
*/
contract("JAGOTOKENV2", (accounts) => {
  //let simpleContract;

  //beforeEach(deploy);
  it("check Token Name", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const result = await jagoInstance.name();
    assert.equal(result, "JAGOTOKEN", "wrong Name of token");

  });

  it("check Token symbol", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const result = await jagoInstance.symbol();
    assert.equal(result, "JAGO", "wrong symbol of token");

  });

  it("check Decimals", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const result = await jagoInstance.decimals();
    assert.equal(result, 9, "wrong decimals");

  });

  it("check totalSupply", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const result = await jagoInstance.totalSupply();
    assert.equal(result, 50000000000000000, "wrong totalSupply");

  });

  it("check balance of token owner", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const owner = '0xC1CC2d14179A1e10934044d452B32b89032B17a5';

    const result = await jagoInstance.balanceOf(owner);
    assert.equal(result, 50000000000000000, "wrong initial balance of owner");

  });

  it("check change tax", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    await jagoInstance.setTax(10);
    const result = await jagoInstance.getTax();
    assert.equal(result, 10, "wrong tax");

  });

  it("check address is excluded from fee", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const addressToExclude = '0x476d4BcF6D445B6b2d46e1ED791f54FA681CE5F8';

    await jagoInstance.changeAddressExcludedFromFee(addressToExclude, true);
    const result = await jagoInstance.isAddressExcludedFromFee(addressToExclude);
    assert.equal(result, true, "excluding address from fee fail!");

  });

  it("check address is member of tokenOwner", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const addressOwnerOfToken = '0x476d4BcF6D445B6b2d46e1ED791f54FA681CE5F8';

    await jagoInstance.changeTokenOwner(addressOwnerOfToken, true);
    const result = await jagoInstance.isAddressTokenOwner(addressOwnerOfToken);
    assert.equal(result, true, "including address to Owner of Token");
    
  });
  
  it("check Minting new token", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const owner = '0xC1CC2d14179A1e10934044d452B32b89032B17a5';
    const mintingAmount = 10;

    const initialBalance = await jagoInstance.balanceOf(owner);
    await jagoInstance.mint(owner, mintingAmount);
    const finalBalance = initialBalance + mintingAmount
    assert.equal(Number.parseInt(finalBalance.toString()), Number.parseInt((initialBalance + mintingAmount).toString()), "minting fail !");

  });

   
  it("check Burning a token", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const owner = '0xC1CC2d14179A1e10934044d452B32b89032B17a5';
    const burnAmount = 100;

    const initialBalance = await jagoInstance.balanceOf(owner);
    await jagoInstance.burn(owner, burnAmount);
    const finalBalance = initialBalance - burnAmount
    assert.equal(Number.parseInt(finalBalance.toString()), initialBalance - burnAmount, "burning fail !");

  });

  
  it("check distributeToken", async () => {
    const acc_presale = '0xE98BfDd71Ba07FB25295e7A36044b0326178835c';
    const acc_privsale = '0x569c24A4f447b26A0CAfB30402aa07A160B31074';
    const acc_marketing = '0xEd4cC38B4e19071646F5ed643155dCec110e9F85';
    const amountForAcc1 = 2400;
    const amountForAcc2 = 2000;
    const amountForAcc3 = 3000;

    const acc = [acc_presale, acc_privsale, acc_marketing];
    const amount = [amountForAcc1, amountForAcc2, amountForAcc3];

    const jagoInstance = await JAGOTOKENV2.deployed();

    await jagoInstance.distributeToken(acc,amount)
    const balPresale = await jagoInstance.balanceOf(acc_presale);
    const balPrivSale = await jagoInstance.balanceOf(acc_privsale);
    const balMarketing = await jagoInstance.balanceOf(acc_marketing);

    //console.log(balPresale, balPrivSale, balMarketing);
    assert.equal(Number.parseInt(balPresale.toString()), amountForAcc1, "address presale received "+amountForAcc1+" token");
    assert.equal(Number.parseInt(balPrivSale.toString()), amountForAcc2, "address presale received "+amountForAcc2+" token");
    assert.equal(Number.parseInt(balMarketing.toString()), amountForAcc3, "address marketing received "+amountForAcc3+" token");
  });

  it("check airdrop Token", async () => {
    const acc1 = '0x96A1D81971C4c9dd10cA0549C1d442BEA471f96f';
    const acc2 = '0xA37BDc90C8f7228CE6fC4F149b7422a8338A4c3d';
    const acc3 = '0x30Cf826F91c999EDc58F868294E8a51659beA165';
    const amount = 10;
    
    const acc = [acc1, acc2, acc3];

    const jagoInstance = await JAGOTOKENV2.deployed();

    await jagoInstance.sendAirdrop(acc,amount);
    const balAcc1 = await jagoInstance.balanceOf(acc1);
    const balAcc2 = await jagoInstance.balanceOf(acc2);
    const balAcc3 = await jagoInstance.balanceOf(acc3);

    assert.equal(Number.parseInt(balAcc1.toString()), amount, "airdrop for acc1 : "+amount+" token");
    assert.equal(Number.parseInt(balAcc2.toString()), amount, "airdrop for acc2 : "+amount+" token");
    assert.equal(Number.parseInt(balAcc3.toString()), amount, "airdrop for acc3 : "+amount+" token");
  
  });


  it("check 1 : transfer token from ownerToken to non excluded address", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const nonExcludedAddr = accounts[2];
    const baseAmountTransfer = 1000;
    const taxDivider = 100;
    const tax = await jagoInstance.getTax(); //10

    let FinalAmountTransfer = 0;
    if(tax > 0) {
      fee = tax * baseAmountTransfer / taxDivider;    //fee = 10*1000/100 = 100
      FinalAmountTransfer = baseAmountTransfer - fee;  // 900
    }

    const initial_balance_nonExclAddr = await jagoInstance.balanceOf(nonExcludedAddr);  //0
    await jagoInstance.transfer(nonExcludedAddr, baseAmountTransfer);                       //transfer 900
    const final_balance_nonExclAddr = await jagoInstance.balanceOf(nonExcludedAddr);    //bal 900
    //console.log(initial_balance_nonExclAddr, final_balance_nonExclAddr);

    assert.equal(
      Number.parseInt(final_balance_nonExclAddr.toString()), 
      Number.parseInt(initial_balance_nonExclAddr.toString()) + FinalAmountTransfer,
      "transfer fail "
    );
    

  });

  it("check 2 : transfer token from ownerToken to excluded address", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const baseAmountTransfer = 1000;
    const taxDivider = 100;
    const tax = await jagoInstance.getTax(); //10

    const addressToExclude = accounts[3];

    await jagoInstance.changeAddressExcludedFromFee(addressToExclude, true);
    const boolResult = await jagoInstance.isAddressExcludedFromFee(addressToExclude);

    let FinalAmountTransfer = 0;
    if(boolResult != true && tax > 0) {
      fee = tax * baseAmountTransfer / taxDivider;    //fee = 10*1000/100 = 100
      FinalAmountTransfer = baseAmountTransfer - fee;  // 900
    }

    const initial_balance_nonExclAddr = await jagoInstance.balanceOf(addressToExclude);  //0
    await jagoInstance.transfer(addressToExclude, baseAmountTransfer);                       //transfer 900
    const final_balance_nonExclAddr = await jagoInstance.balanceOf(addressToExclude);    //bal 900
    //console.log(initial_balance_nonExclAddr, final_balance_nonExclAddr);
    
    assert.equal(
      Number.parseInt(final_balance_nonExclAddr.toString()), 
      Number.parseInt(initial_balance_nonExclAddr.toString()) + baseAmountTransfer,
      "transfer fail "
    );

  });
  it("check transfer token from between ordinary address", async () => {
    //tidak dapat dibuktikan karena tidak bisa mengubah msg.sender()
    const jagoInstance = await JAGOTOKENV2.deployed();
    const addressTo = accounts[5];
    const baseAmountTransfer = 1000;  
    console.log(addressTo);

    const init_balance = await jagoInstance.balanceOf(addressTo); 
    await jagoInstance.transfer(addressTo, baseAmountTransfer);                    
    const final_balance = await jagoInstance.balanceOf(addressTo);  

    assert.equal(
      Number.parseInt(final_balance.toString()), 
      Number.parseInt(init_balance.toString()) + baseAmountTransfer,
      "transfer fail "
    );

  });
  //approve
  it("check approve amount of token to an anddress(spender)", async () => {
    const jagoInstance = await JAGOTOKENV2.deployed();
    const addressSpender = accounts[1];
    const amount = 100;

    await jagoInstance.approve(addressSpender, amount)

    const amountCanBeSpend = await jagoInstance.allowance(accounts[0], addressSpender);

    assert.equal(amountCanBeSpend,amount, "approving allowance fail "+amountCanBeSpend+" ");
  });

  //allowance

});
